/*
Run the example:
$ make

Create a desired image format (ImageMagick):
$ convert -resize 256x256 -colorspace Gray lena.png lena.bmp
*/

#include <stdio.h>
#include <stdlib.h>
#include "cbmp.h"


int main() {
    BMP* bmp = bopen("files/lena.bmp");

    unsigned int x, y, width, height;
    unsigned char r, g, b;

    width = get_width(bmp);
    height = get_height(bmp);
    for (x = 0; x < width; x++) {
        for (y = 0; y < height; y++)  {
            get_pixel_rgb(bmp, x, y, &r, &g, &b);
            r = r / 2;
            set_pixel_rgb(bmp, x, y, r, r, r);
        }
    }

    bwrite(bmp, "files/lena_out.bmp");
    bclose(bmp);
}